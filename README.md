# **Benance**
SENIOR PYTHON DEVELOPER CODING TEST - OUTPUT

Benance - Simple Trading System

## Coding test guidelines
Please build a simple trading system as a pure REST API with the endpoints outlined below. We want to
allow authenticated users the ability to place orders to buy and sell stocks and track the overall value of their
investments. Stocks will have an id, name, and price.

- Create an endpoint to let users place trades. When an order is placed, we need to record the
quantity of the stock the user wants to buy or sell.
- Create an endpoint to retrieve the total value invested in a single stock by a user. To calculate this, we need to sum all the value of all orders placed by the user for a single stock. Order value is
calculated by multiplying quantity and stock price.
-  Create an endpoint to retrieve the total value invested in a single stock by a user.
- Create an endpoint to search for stocks by name
- Any details beyond what is outlined is up to your discretion.

## Requirements

- python 3.6+
- virtualenv
- pip3
- postgresql 10.3+

## Initial setup

  cd git/
  git clone https://gitlab.com/benjoazucena/benance.git
  cd benance/
  virtualenv --python python3 --no-site-packages venv
  source venv/bin/activate
  pip install -r requirements.pip

## Python dependencies
  
  see requirements.pip

## Configure

  see sample config file at etc/webapp.config.source

## Database setup

  $dbuser = benance
  $dbname = benance_db
  ---

  bash $ createuser --createdb --pwprompt $dbuser
  bash $ createdb createdb -E UTF8 -O $dbuser $dbname

  --- alternative
  CREATE DATABASE $dbname WITH OWNER = $dbuser ENCODING = 'UTF8'; 
  CREATE EXTENSION IF NOT EXISTS hstore;

  // for tests to work, add hstore to template1
  psql template1 -c 'create extension hstore;'

  ---
  test the connection:
    psql -U $dbuser -h $dbhost -p 5432 $dbname
    
  cheatsheets: 
    https://gist.github.com/apolloclark/ea5466d5929e63043dcf
    https://gist.github.com/clhenrick/ebc8dc779fb6f5ee6a88

## API Documentation
https://docs.google.com/document/d/1BP5po4ygCqPWNLmT3CgPa-lj_dBqKs0Pfd39MlW2IsQ/edit

## System Demo
The video privacy is set to unlisted. Only user with the link can view this.

https://youtu.be/7a0HFakBKrM
