from ..models import Stock


def validate_currency(value):
    """ Returns the float is string is a number. """
    try:
        value = float(value)
        if value > 0:
            return value
    except ValueError:
        return False

    return False


def validate_order_type(value):
    """ Returns the string if valid order type. """
    if value.strip().upper() == "SELL" or value.strip().upper() == "BUY":
        return value.strip().upper()

    return False


def validate_stock_symbol(value):
    valid_stock_symbol = value.strip().upper()
    if not Stock.objects.filter(symbol=valid_stock_symbol).exists():
        return False

    return valid_stock_symbol

