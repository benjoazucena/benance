# Generated by Django 3.2.6 on 2021-08-08 12:15

from django.conf import settings
import django.contrib.postgres.fields.hstore
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', django_extensions.db.fields.ShortUUIDField(blank=True, editable=False, unique=True)),
                ('name', models.CharField(blank=True, default=None, max_length=100, null=True)),
                ('symbol', models.CharField(db_index=True, max_length=6, unique=True)),
                ('price', models.FloatField(default=0)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified', models.DateTimeField(auto_now=True, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='Trade',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', django_extensions.db.fields.ShortUUIDField(blank=True, editable=False, unique=True)),
                ('order_type', models.CharField(choices=[('SELL', 'SELL'), ('BUY', 'BUY')], max_length=4)),
                ('shares', models.FloatField(default=0)),
                ('price_in_time', models.FloatField(default=0)),
                ('is_reflected', models.BooleanField(blank=True, default=False, null=True)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified', models.DateTimeField(auto_now=True, db_index=True)),
                ('stock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stocks', to='trades.stock')),
                ('trader', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='trader', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Portfolio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', django_extensions.db.fields.ShortUUIDField(blank=True, editable=False, unique=True)),
                ('total_profit', models.FloatField(default=0)),
                ('total_value', models.FloatField(default=0)),
                ('holdings', django.contrib.postgres.fields.hstore.HStoreField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('user_account', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user_account', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
