from django.contrib.postgres.search import SearchQuery, SearchRank, TrigramSimilarity, SearchVector
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from .commons import validator
from .models import Ledger, UserPortfolio, Stock

search_vectors = (
    SearchVector('name', weight='A', config='english')
)


@api_view(['POST',])
@permission_classes([IsAuthenticated,])
def make_order(request):
    if request.method == 'POST':
        payload = request.POST
        user = request.user
        valid_stock_symbol = validator.validate_stock_symbol(payload.get("stock_symbol"))
        valid_order_type = validator.validate_order_type(payload.get("order_type"))
        valid_amount = validator.validate_currency(payload.get("amount"))
        valid_shares = validator.validate_currency(payload.get("shares"))
        if not valid_amount and not valid_shares :
            return JsonResponse({'message': 'Please enter a valid number of shares or amount'}, status=400)
        if valid_amount and valid_shares :
            return JsonResponse({'message': 'Please select only one between number of shares and amount '}, status=400)
        if not valid_order_type:
            return JsonResponse({'message': 'Please enter a valid order type'}, status=400)
        if not valid_stock_symbol:
            return JsonResponse({'message': 'Enter a valid stock symbol'}, status=400)
        user_portfolio = UserPortfolio.objects.get(user_account=user)
        result = user_portfolio.submit_order(
            stock_symbol=valid_stock_symbol,
            order_type=valid_order_type,
            amount=valid_amount,
            shares=valid_shares
        )

    return result


@api_view(['POST',])
@permission_classes([IsAuthenticated,])
def get_total_value_invested(request):
    if request.method == 'POST':
        payload = request.POST
        user = request.user
        valid_stock_symbol = validator.validate_stock_symbol(payload.get("stock_symbol"))
        if not valid_stock_symbol:
            return JsonResponse({'message': 'Enter a valid stock symbol'}, status=400)
        user_portfolio = UserPortfolio.objects.get(user_account=user)
        return user_portfolio.get_total_value_invested(valid_stock_symbol)


@api_view(['POST',])
@permission_classes([IsAuthenticated,])
def search_stock_by_name(request):
    try:
        if request.method == 'POST':
            payload = request.POST
        query = payload.get("name")

        result_list = list()
        category = ""

        search_query = SearchQuery(query, config='english')
        search_rank = SearchRank(search_vectors, search_query)
        trigram_similarity = TrigramSimilarity('name', query)
        print(trigram_similarity)
        search_results = Stock.objects.annotate(
            search=search_vectors
        ).annotate(
            rank=search_rank + trigram_similarity
        ).filter(
            rank__gte=0.5,
        ).order_by('-rank')

        print(search_results)
        result_list.append(search_results.first().as_search_dict())

        print(result_list)
        return JsonResponse(result_list, safe=False)

    except Exception as e:
        return JsonResponse({'message': 'Invalid Request'}, status=400)