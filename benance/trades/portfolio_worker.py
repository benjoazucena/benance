import sys
import logging
import multiprocessing as mp
import sys
import time
from datetime import datetime

from django import db
from django.db import transaction

from .models import Ledger

logger = logging.getLogger(__name__)


class PortfolioWorker(object):

    def __init__(self,
                 idle_sleep_sec=5,
                 db_src='processor01', db_dest='processor02'):
        self.idle_sleep_sec = idle_sleep_sec
        self.db_src = db_src
        self.db_dest = db_dest

    def calculate_portfolio(self, message, now=None):
      pass

    def run_once(self):
        db.connections.close_all()
        with transaction.atomic(using=self.db_src):
            message = Ledger.fetch_one_lock_start(using=self.db_src)
            if not message:
                logger.debug('idle, sleeping ...')
                time.sleep(self.idle_sleep_sec)
            else:
                now = datetime.now()
                self.calculate_portfolio(message, now=now)
        db.connections.close_all()

    def run(self):
        mp.set_start_method('fork')

        # run loop
        while True:
            p = mp.Process(target=lambda: self.run_once())
            p.start()
            p.join()
            if p.exitcode != 0:
                sys.exit(1)