from django.conf.urls import url

from . import views

urlpatterns = [
    # APIs
    url(
        regex=r'^order/?$',
        view=views.make_order,
        name='make_order'
    ),
    url(
        regex=r'^get-total-value-invested/?$',
        view=views.get_total_value_invested,
        name='get_total_value_invested'
    ),
url(
        regex=r'^search-stock-by-name/?$',
        view=views.search_stock_by_name,
        name='search_stock_by_name'
    ),
]