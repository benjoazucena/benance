from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import HStoreField
from django.db import models
from django.db.models import Sum
from django.http import JsonResponse
from django_extensions.db.fields import ShortUUIDField

UserModel = get_user_model()


class Stock(models.Model):
    guid = ShortUUIDField(unique=True)
    name = models.CharField(max_length=100, default=None, blank=True, null=True)
    symbol = models.CharField(max_length=6, null=False, db_index=True, unique=True)
    price = models.FloatField(default=0)
    
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True, db_index=True)

    def __str__(self):
        return "Stock ({0}): {1}".format(self.id, self.name)

    def convert_amount_to_shares(self, amount):
        return amount/self.price

    def as_search_dict(self):
        d = {}
        d['guid'] = self.guid
        d['name'] = self.name
        d['symbol'] = self.symbol
        d['price'] = self.price
        d['created'] = self.created
        d['modified'] = self.modified

        return d


class Ledger(models.Model):
    ORDER_CHOICES = (
        ('SELL', 'SELL'),
        ('BUY', 'BUY')
    )
    guid = ShortUUIDField(unique=True)
    stock = models.ForeignKey(Stock, related_name='ledger_records', on_delete=models.CASCADE, db_index=True)
    trader = models.ForeignKey(UserModel, related_name='ledger_records', on_delete=models.CASCADE, db_index=True)
    order_type = models.CharField(max_length=4, choices=ORDER_CHOICES, blank=False)
    shares = models.FloatField(default=0)
    price_in_time = models.FloatField(default=0)
    is_reflected = models.BooleanField(default=False, null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True, db_index=True)

    def __str__(self):
        return "Trade {0} {1} {2} {3}".format(self.order_type, self.stock, self.trader, self.shares)

    @staticmethod
    def fetch_one_lock_start(using='default', guid=None):
        query_filters = {
            "is_reflected": False,
        }
        trade = Ledger.objects.using(using) \
            .filter(**query_filters).first()
        return trade

    @staticmethod
    def create_new_record(stock, trader=None, order_type=None, shares=None, price_in_time=None):
        if order_type == "SELL":
            print(trader)
            trades = trader.ledger_records.filter(stock=stock)
            total_shares_bought = 0
            total_shares_sold = 0
            for trade in trades:
                if trade.order_type == "BUY":
                    total_shares_bought += trade.shares
                else:
                    total_shares_sold += trade.shares

            if total_shares_bought - total_shares_sold < shares:
                return JsonResponse({'message': 'Sell order exceeds stock holdings'}, status=400)

        try:
            Ledger.objects.create(
                stock=stock,
                trader=trader,
                order_type=order_type,
                shares=shares,
                price_in_time=price_in_time,
            )
        except Exception as e:
            print(e)
            return JsonResponse({'message': 'There was a problem in create the record'}, status=400)
        return JsonResponse({'message': 'The trade has been successfully processed'}, status=200)


class UserPortfolio(models.Model):
    guid = ShortUUIDField(unique=True)
    user_account = models.OneToOneField(UserModel, related_name='user_account', on_delete=models.CASCADE, db_index=True)
    total_profit = models.FloatField(default=0)
    total_value = models.FloatField(default=0)
    holdings = HStoreField(default=None)

    created = models.DateTimeField(auto_now_add=True, db_index=False)
    modified = models.DateTimeField(auto_now=True, db_index=False)

    def __str__(self):
        return "Portfolio ({0}): {1} : {2}".format(self.guid, self.user_account, self.total_value)

    def submit_order(self, stock_symbol="", order_type="", amount="", shares=""):
        stock = Stock.objects.get(symbol=stock_symbol)
        if amount:
            shares = stock.convert_amount_to_shares(amount)

        return Ledger.create_new_record(
            stock=stock,
            trader=self.user_account,
            order_type=order_type,
            shares=shares,
            price_in_time=stock.price,
        )

    def get_total_value_invested(self, stock_symbol):
        stock = Stock.objects.get(symbol=stock_symbol)
        trades = self.user_account.ledger_records.filter(stock=stock)
        bought_shares_value = 0.0
        for trade in trades:
            if trade.order_type == "BUY":
                bought_shares_value += trade.shares* trade.price_in_time
        return JsonResponse({'message': 'Total Value Invested = %s' % bought_shares_value}, status=200)


