from django.contrib import admin
from .models import Ledger, Stock, UserPortfolio


class StockAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'name', 'price', 'created', 'modified')


admin.site.register(Stock, StockAdmin)


class LedgerAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'stock', 'trader', 'order_type', 'shares', 'price_in_time', 'is_reflected',
                    'created', 'modified')


admin.site.register(Ledger, LedgerAdmin)


class UserPortfolioAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'user_account', 'total_profit', 'total_value', 'holdings', 'created', 'modified')


admin.site.register(UserPortfolio, UserPortfolioAdmin)